class ModelProduct{
  final String id;
  final String title;
  final String categoryId;
  final int cost;
  final String description;
  final bool isBestSeller;
  final List<String> imageList;
  bool isFavorite;
  bool isCart;

  ModelProduct({required this.id, required this.title, required this.categoryId, required this.cost, required this.description, required this.isBestSeller, required this.imageList, this.isFavorite = false, this.isCart = false});
}