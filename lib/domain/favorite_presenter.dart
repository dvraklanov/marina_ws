import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/data/storage/products.dart';

void insertFavorite(ModelProduct product){
  var index = allProducts.indexOf(product);
  product.isFavorite = !product.isFavorite;
  allProducts[index] = product;
}

List<ModelProduct> getFavoriteProducts(){
  var res = allProducts.where((element) => element.isFavorite == true).toList();
  return res;
}