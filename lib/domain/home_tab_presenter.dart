import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/data/storage/products.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> getCategoryList(Function onResponse, Function(String) onError) async {
  try{
    var res = await getCategory();
    categories = res;
    onResponse(res);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

Future<void> getAllProductsFunc(Function onResponse, Function(String) onError) async {
  try{
    var res = await getProducts();
    for(int i =0; i<res.length; i++){
      List<String> images = await getImagesForProduct(res[i]['id']);
      allProducts.add(ModelProduct(id: res[i]['id'], title: res[i]['title'], categoryId: res[i]['category_id'], cost: res[i]['cost'], description: res[i]['description'], isBestSeller: res[i]['is_best_seller'], imageList: images));
    }
    onResponse(true);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

List<ModelProduct> getPopularProducts(){
  return allProducts.where((element) => element.isBestSeller == true).toList();
}

bool getStateUser(){
  return currentUser();
}