import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressSignIn(String email, String password, Function onResponse, Function(String) onError) async {
  try{
    if(!checkEmail(email)){
      onError('Email not valid');
      return;
    }
    if(!checkPassword(password)){
      onError('Input Password');
      return;
    }
    await signIn(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}