import 'package:flutter/material.dart';
import 'package:prof2024/presentation/pages/cart_page_2.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FilledButton(
          onPressed: () { 
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CartPage2()));
          }, 
          child: Text('GO'),
        ),
      ),
    );
  }
}