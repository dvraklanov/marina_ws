import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/cart_presenter.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/theme/colors.dart';

import '../widgets/dialogs.dart';

class CartPage2 extends StatefulWidget {
  const CartPage2({super.key});

  @override
  State<CartPage2> createState() => _CartPage2State();
}

class _CartPage2State extends State<CartPage2> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.only(top: 48.w, left: 20.w, right: 20.w),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    height: 44.w,
                    width: 44.w,
                    decoration: BoxDecoration(
                        color: colors.block,
                        borderRadius: BorderRadius.circular(360)
                    ),
                    child: SvgPicture.asset('assets/back.svg', fit: BoxFit.scaleDown,),
                  ),
                ),
                Text(
                  'Корзина',
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 44.w,
                  width: 44.w,),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: 50.w,
                width: double.infinity,
                child: FilledButton(
                    key: const Key('Button'),
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Home()));
                    },
                    style: Theme.of(context).filledButtonTheme.style,
                    child: const Text('Подтвердить')
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}