import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/domain/cart_presenter.dart';
import 'package:prof2024/domain/details_presenter.dart';
import 'package:prof2024/domain/favorite_presenter.dart';
import 'package:prof2024/presentation/theme/colors.dart';

class DetailsPage extends StatefulWidget {
  final ModelProduct product;
  const DetailsPage({super.key, required this.product});

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  int index = 0;

  List<Widget> createRow(){
    var colors = LightColorsApp();
    List<Widget> widgets = [];
    for(int i=0;i<widget.product.imageList.length; i++){
      widgets.add(
        Row(
          children: [
            GestureDetector(
              onTap: (){
                setState(() {
                  index = i;
                });
              },
              child: Container(
                height: 56.w,
                width: 56.w,
                decoration: BoxDecoration(
                  color: colors.block,
                  borderRadius: BorderRadius.circular(16)
                ),
                child: AspectRatio(
                  aspectRatio: 52.w/27.w,
                  child: CachedNetworkImage(
                    imageUrl: widget.product.imageList[i],
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(width: 14.w)
          ],
        )
      );
    }
    return widgets;
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 48.w, left: 20.w, right: 20.w),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: 44.w,
                          width: 44.w,
                          decoration: BoxDecoration(
                              color: colors.block,
                            borderRadius: BorderRadius.circular(360)
                          ),
                          child: SvgPicture.asset('assets/back.svg', fit: BoxFit.scaleDown,),
                        ),
                      ),
                      Text(
                        'Sneaker Shop',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600),
                      ),
                      Container(
                        height: 44.w,
                        width: 44.w,
                        decoration: BoxDecoration(
                            color: colors.block,
                            borderRadius: BorderRadius.circular(360)
                        ),
                        child: SvgPicture.asset('assets/bag.svg', fit: BoxFit.scaleDown, color: colors.text),
                      ),
                    ],
                  ),
                  SizedBox(height: 26.w),
                  Row(
                    children: [
                      Flexible(
                        child: Text(
                          widget.product.title,
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ),
                      SizedBox(width: 96.w)
                    ],
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    getCategoryString(widget.product.categoryId),
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(color: colors.hint),
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    "₽${widget.product.cost.toString()}.00",
                    style: Theme.of(context).textTheme.bodyMedium,
                  )
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(top: 185.w, left: 47.w, right: 47.w),
                child: CachedNetworkImage(
                    imageUrl: widget.product.imageList[index],
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 364.w, left: 12.w, right: 12.w),
                child: SvgPicture.asset('assets/slider.svg', fit: BoxFit.cover,),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 24.w, top: 469.w),
                child: SizedBox(
                  height: 56.w,
                  width: double.infinity,
                  child: Row(
                    children: <Widget>[]+createRow(),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 558.w),
                child: Column(
                  children: [
                    Text(
                      widget.product.description,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                    SizedBox(
                      height: 9.w,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          'Подробнее',
                          style: Theme.of(context).textTheme.bodySmall?.copyWith(color: colors.accent, height: 21.w/14.w),
                        ),
                      ],
                    ),
                    SizedBox(height: 60.w),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              insertFavorite(widget.product);
                            });
                          },
                          child: Container(
                            height: 52.w,
                            width: 52.w,
                            decoration: BoxDecoration(
                              color: colors.cont,
                              borderRadius: BorderRadius.circular(360)
                            ),
                            child: (widget.product.isFavorite) ? SvgPicture.asset('assets/Icon.svg', fit: BoxFit.scaleDown,) : SvgPicture.asset('assets/heart.svg', color: colors.text, fit: BoxFit.scaleDown,),
                          ),
                        ),
                        SizedBox(width: 18.w),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              insertCart(widget.product);
                            });
                          },
                          child: Container(
                            height: 52.w,
                            width: 265.w,
                            decoration: BoxDecoration(
                              color: colors.accent,
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Row(
                              children: [
                                SizedBox(width: 12.w),
                                SvgPicture.asset('assets/bag-2.svg', color: colors.block, fit: BoxFit.scaleDown,),
                                Expanded(
                                    child: Text(
                                        (widget.product.isCart) ? 'Добавлено' : 'В корзину',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w600, color: colors.block, height: 22.w/14.w),
                                    )
                                ),
                                SizedBox(width: 12.w,)
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}