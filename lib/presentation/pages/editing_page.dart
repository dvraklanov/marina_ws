import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/profile_tab_presenter.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field_page.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';


class EditingPage extends StatefulWidget {
  const EditingPage({super.key});

  @override
  State<EditingPage> createState() => _EditingPageState();
}

class _EditingPageState extends State<EditingPage> {
  Uint8List? avatar;
  String avatarString = '';

  @override
  void initState(){
    super.initState();
    data = getData()!;
    name = TextEditingController(text: data['name']);
    secondName = TextEditingController(text: data['secondName']);
    phone = TextEditingController(text: data['phone']);
    secondName = TextEditingController(text: data['address']);
    avatarString = data['avatar'];
    setState(() {});
  }
  Map<String, dynamic> data = {};
  TextEditingController name = TextEditingController();
  TextEditingController secondName = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController address = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 55.w,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 32.w,
                  width: 212.w,
                  child: FilledButton(
                      onPressed: (){
                        pressUpdate(name.text, secondName.text, address.text, phone.text, (){Navigator.of(context).pop();}, (String e){showError(context, e);});
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text('Сохранить')
                  ),
                ),
                SizedBox(height: 43.w),
                Container(
                  height: 96.w,
                  width: 96.w,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(360),
                    child: (avatar == null) ? (avatarString == '') ? Image.network(avatarString, fit: BoxFit.cover,) : SizedBox() : Image.memory(avatar!, fit: BoxFit.cover,),

                  ),
                ),
                SizedBox(height: 15.w),
                Text(
                  'fdffsdf',
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 20.sp, height: 23.48.w/20.w, fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 8.w),
                GestureDetector(
                  onTap: (){
                    showPickAvatar(context, (res){
                      pressUpdateAvatar(res, (){setState(() {
                        avatar = res;
                      });}, (p0) => null
                      );
                    });
                  },
                  child: Text(
                    'Изменить фото профиля',
                    style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w600, color: colors.accent),
                  ),
                )
              ],
            ),
            SizedBox(height: 21.w),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                children: [
                  SizedBox(height: 20.w),
                  CustomTextFieldPage(label: 'Имя', controller: name),
                  SizedBox(height: 16.w),
                  CustomTextFieldPage(label: 'Фамилия', controller: secondName),
                  SizedBox(height: 16.w),
                  CustomTextFieldPage(label: 'Адрес', controller: address),
                  SizedBox(height: 16.w),
                  CustomTextFieldPage(label: 'Телефон', controller: phone, isPhone: true,),
                  SizedBox(height: 100.w)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}