import 'package:change_case/change_case.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/log_in_presenter.dart';
import 'package:prof2024/domain/sign_up_presenter.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/pages/log_in.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController name = TextEditingController();
  bool isConfirm = false;
  bool enableButton = true;
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 66.w, left: 20.w, right: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Home()));
                },
                child: Container(
                  height: 44.w,
                  width: 44.w,
                  decoration: BoxDecoration(
                    color: colors.background,
                    borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/back.svg', color: colors.text, fit: BoxFit.scaleDown,),
                ),
              ),
              SizedBox(height: 11.w,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Регистрация',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    'Заполните Свои Данные Или\n Продолжите Через Социальные Медиа',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  SizedBox(height: 30.w),
                  CustomTextField(
                      label: 'Ваше имя',
                      hint: 'xxxxxxxx',
                      controller: name,
                      letter: 2,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 12.w),
                  CustomTextField(
                      label: 'Email',
                      hint: 'xyz@gmail.com',
                      controller: email,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 12.w),
                  CustomTextField(
                      label: 'Пароль',
                      hint: '••••••••',
                      controller: password,
                      enableObscure: true,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 12.w),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            isConfirm = !isConfirm;
                          });
                        },
                        child: Container(
                          height: 18.w,
                          width: 18.w,
                          decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(6)
                          ),
                          child: (isConfirm) ? Center(child: SvgPicture.asset('assets/shield.svg', width: 10.w, height: 10.w,)) : const SizedBox(),
                        ),
                      ),
                      SizedBox(width: 12.w),
                      GestureDetector(
                        onTap: (){},
                        child: Column(
                          children: [
                            Text(
                              'Даю согласие на обработку \nперсональных данных',
                              maxLines: 2,
                              style: Theme.of(context).textTheme.labelMedium?.copyWith(color: colors.hint,),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 24.w),
                  SizedBox(
                    height: 50.w,
                    width: double.infinity,
                    child: FilledButton(
                        onPressed: (enableButton) ? (){
                          pressSignUp(
                              email.text,
                              password.text,
                              name.text,
                                  (){
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Home()), (route) => false);
                              },
                              (String e){showError(context, e);}
                          );
                        } : null,
                        style: Theme.of(context).filledButtonTheme.style,
                        child: const Text('Зарегистрироваться')
                    ),
                  ),
                  SizedBox(height: 113.w),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => LogIn()));
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Есть аккаунт? ',
                              style: Theme.of(context).textTheme.labelMedium?.copyWith(color: colors.hint)
                          ),
                          TextSpan(
                              text: 'Войти',
                              style: Theme.of(context).textTheme.labelMedium
                          )
                        ]
                    )),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}