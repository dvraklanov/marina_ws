import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/presentation/pages/editing_page.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field_page.dart';

import '../../../domain/profile_tab_presenter.dart';
import '../../theme/theme.dart';
import '../side_menu.dart';

class ProfileTab extends StatefulWidget {
  const ProfileTab({super.key});

  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  Uint8List? avatar;
  String avatarString = '';

  @override
  void initState(){
    super.initState();
    data = getData()!;
    setState(() {});
  }
  Map<String, dynamic> data = {};
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 18.w, top: 60.w, right: 22.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => SideMenu()));
                      },
                      child: SvgPicture.asset('assets/hamburger.svg', width: 25.71.w, height: 18.w,)
                  ),
                  Text(
                    'Профиль',
                    style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditingPage()));
                    },
                    child: Container(
                      height: 25.w,
                      width: 25.w,
                      decoration: BoxDecoration(
                          color: colors.accent,
                          borderRadius: BorderRadius.circular(360)
                      ),
                      child: SvgPicture.asset('assets/pen.svg', color: colors.block, fit: BoxFit.scaleDown,),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 45.w,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 96.w,
                  width: 96.w,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(360),
                    child: (avatar == null) ? (avatarString == '') ? Image.network(avatarString, fit: BoxFit.cover,) : SizedBox() : Image.memory(avatar!, fit: BoxFit.cover,),

                  ),
                ),
                SizedBox(height: 15.w),
                Text(
                  'fdsfsf',
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 20.sp, height: 23.48.w/20.w, fontWeight: FontWeight.w600),
                )
              ],
            ),
            SizedBox(height: 35.w),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                children: [
                  Container(
                    height: 66.w,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: colors.background,
                      borderRadius: BorderRadius.circular(16)
                    ),
                  ),
                  SizedBox(height: 20.w),
                  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Имя',
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, height: 18.78.w/16.w),
                    ),
                    SizedBox(height: 17.w),
                    Container(
                      width: double.infinity,
                      height: 48.w,
                      decoration: BoxDecoration(
                        color: colors.background,
                        borderRadius: BorderRadius.circular(14)
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(width: 16.w,),
                          Text(
                              data['name'],
                              style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w400, color: colors.text, height: 16.44.w/14.w),)
                        ],
                      ),
                    )
                  ],
                ),
                  SizedBox(height: 16.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Фамилия',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, height: 18.78.w/16.w),
                      ),
                      SizedBox(height: 17.w),
                      Container(
                        width: double.infinity,
                        height: 48.w,
                        decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(14)
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(width: 16.w,),
                            Text('fsdfdsf',
                                style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w400, color: colors.text, height: 16.44.w/14.w),)
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 16.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Адрес',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, height: 18.78.w/16.w),
                      ),
                      SizedBox(height: 17.w),
                      Container(
                        width: double.infinity,
                        height: 48.w,
                        decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(14)
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(width: 16.w,),
                            Text(
                                'fdsfsfd',
                                style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w400, color: colors.text, height: 16.44.w/14.w))
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 16.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Телефон',
                        style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, height: 18.78.w/16.w),
                      ),
                      SizedBox(height: 17.w),
                      Container(
                        width: double.infinity,
                        height: 48.w,
                        decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(14)
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(width: 16.w,),
                            Text(
                                'gfdgdg',
                                style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w400, color: colors.text, height: 16.44.w/14.w),)
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 100.w)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}