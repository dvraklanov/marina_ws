import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/data/models/ModelProduct.dart';
import 'package:prof2024/domain/cart_presenter.dart';
import 'package:prof2024/presentation/pages/details.dart';
import 'package:prof2024/presentation/theme/colors.dart';

import '../../domain/favorite_presenter.dart';

class CustomCard extends StatefulWidget {
  final ModelProduct product;
  const CustomCard({super.key, required this.product});

  @override
  State<CustomCard> createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Container(
      height: 182.w,
      width: 160.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: colors.block
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              Stack(
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 9.w, right: 9.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          AspectRatio(
                            aspectRatio: 117.91.w/70.w,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailsPage(product: widget.product)));
                              },
                              child: CachedNetworkImage(
                                  imageUrl: widget.product.imageList[0],
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 9.w, left: 9.w),
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          insertFavorite(widget.product);
                        });
                      },
                      child: Container(
                        height: 24,
                        width: 24,
                        decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(360)
                        ),
                        child: (widget.product.isFavorite) ? SvgPicture.asset('assets/Icon.svg', fit: BoxFit.scaleDown, width: 16.w, height: 16.w,) : SvgPicture.asset('assets/heart.svg', fit: BoxFit.scaleDown,),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 9.w, top: 100.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (widget.product.isBestSeller) ? 'Best Seller' : '',
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w500, color: colors.accent),
                    ),
                    SizedBox(height: 8.w),
                    Text(
                      widget.product.title,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, color: colors.hint),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Stack(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 114.8.w),
                              child: GestureDetector(
                                onTap: (){
                                  insertCart(widget.product);
                                  setState(() {});
                                },
                                child: Container(
                                  height: 34.w,
                                  width: 34.w,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: colors.accent,
                                      borderRadius: const BorderRadius.only(topLeft: Radius.circular(16), bottomRight: Radius.circular(16))
                                  ),
                                  child: (widget.product.isCart) ?  SvgPicture.asset('assets/cart.svg', fit: BoxFit.scaleDown,):SvgPicture.asset('assets/add.svg', fit: BoxFit.scaleDown,),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 14.w),
                              child: Text(
                                '₽${widget.product.cost.toString()}.00',
                                style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.hint),
                              ),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
