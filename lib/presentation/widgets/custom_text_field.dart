import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final bool enableObscure;
  final TextEditingController controller;
  final Function(String) onChanged;
  final double letter;
  const CustomTextField({super.key, required this.label, required this.hint, this.enableObscure = false, required this.controller, required this.onChanged, this.letter = 0});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}
bool isObscure = true;
class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleSmall,
        ),
        SizedBox(height: 12.w),
        SizedBox(
          width: double.infinity,
          height: 48.w,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: '*',
            onSubmitted: widget.onChanged,
            decoration: InputDecoration(
              hintText: widget.hint,
              enabledBorder: Theme.of(context).inputDecorationTheme.enabledBorder,
              focusedBorder: Theme.of(context).inputDecorationTheme.focusedBorder,
              hintStyle: Theme.of(context).textTheme.labelSmall?.copyWith(letterSpacing: widget.letter),
              suffixIcon: (widget.enableObscure) ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                  child: SvgPicture.asset('assets/Eye.svg', width: 20.w, height: 20.w, fit: BoxFit.scaleDown,)
              ) : null
            ),
          ),
        )
      ],
    );
  }
}
