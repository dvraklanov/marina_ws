import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2024/presentation/theme/colors.dart';

class CustomTextFieldPage extends StatefulWidget {
  final String label;
  final TextEditingController controller;
  final bool isPhone;
  const CustomTextFieldPage({super.key, required this.label, required this.controller, this.isPhone = false});

  @override
  State<CustomTextFieldPage> createState() => _CustomTextFieldPageState();
}
bool isObscure = true;
class _CustomTextFieldPageState extends State<CustomTextFieldPage> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.w600, height: 18.78.w/16.w),
        ),
        SizedBox(height: 17.w),
        SizedBox(
          width: double.infinity,
          height: 48.w,
          child: TextField(
            keyboardType: (widget.isPhone) ? TextInputType.phone : TextInputType.text,
            controller: widget.controller,
            style: Theme.of(context).textTheme.labelSmall?.copyWith(fontWeight: FontWeight.w400, color: colors.text, height: 16.44.w/14.w),
            decoration: InputDecoration(
                border: Theme.of(context).inputDecorationTheme.border,
                enabledBorder: Theme.of(context).inputDecorationTheme.enabledBorder,
                focusedBorder: Theme.of(context).inputDecorationTheme.focusedBorder,
                suffixIcon: SvgPicture.asset('assets/confirm.svg', fit: BoxFit.scaleDown)
            ),
          ),
        )
      ],
    );
  }
}
